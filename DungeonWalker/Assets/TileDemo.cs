﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileDemo : MonoBehaviour {

    [SerializeField, Tooltip("Width in tiles")]
    int width;

    [SerializeField, Tooltip("Height in tiles")]
    int height;

    [SerializeField]
    Texture2D tileSet;

    [SerializeField]
    float percentWidthMax = 0.9f;
    [SerializeField]
    float percentWidthMin = 0.7f;

    [SerializeField]
    float percentHeightMax = 0.9f;
    [SerializeField]
    float percentHeightMin = 0.7f;

    public Sprite[] allTiles;

    public int[] map;

    GameObject[] tileObjectList;

	// Use this for initialization
	void Start ()
    {
        allTiles = Resources.LoadAll<Sprite>(tileSet.name);

        GenerateMap();

        GenerateTileObjects();
        AssignTileSprites();
	}

    private void AssignTileSprites()
    {
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                int i = col + row * width;

                tileObjectList[i].GetComponent<SpriteRenderer>().sprite = allTiles[map[i]];
            }
        }
    }

    private void GenerateMap()
    {
        map = new int[width * height];

        //first pass
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                map[col + row * width] = Random.Range(0, 3);
            }
        }

        //second pass
        int roomWidth = Mathf.FloorToInt(width * Random.Range(percentWidthMin, percentWidthMax));
        int roomHeight = Mathf.FloorToInt(width * Random.Range(percentHeightMin, percentHeightMax));

        roomWidth = Mathf.Clamp(roomWidth, 1, width - 1);
        roomHeight = Mathf.Clamp(roomHeight, 1, height - 1);

        int startX = (width - roomWidth) / 2;
        int startY = (height - roomHeight) / 2;

        for (int row = startY; row < roomHeight; row++)
        {
            for (int col = startX; col < roomWidth; col++)
            {
                map[col + row * width] = Random.Range(3, 6);
            }
        }

        //third pass
        bool left = false;
        if(Random.value > 0.5f)
        {
            left = true;
        }

        if(left)
        {
            bool gapDone = false;
            int splitPoint = roomHeight / 2 + startY;
            for(int col = startX; col < (startX + roomWidth); col++)
            {
                if(!gapDone)
                {
                    if(Random.value < .15)
                    {
                        gapDone = true;
                    }
                    else
                    {
                        map[col + splitPoint * width] = Random.Range(0, 3);
                    }
                }
                else
                {
                    map[col + splitPoint * width] = Random.Range(0, 3);
                }
            }

            if(!gapDone)
            {
                map[startX + splitPoint * width] = Random.Range(3, 6);
            }
        }

        else
        {
            bool gapDone = false;
            int splitPoint = roomWidth / 2 + startX;
            for (int row = startY; row < (startY + roomHeight); row++)
            {
                if (!gapDone)
                {
                    if (Random.value < .15)
                    {
                        gapDone = true;
                    }
                    else
                    {
                        map[splitPoint + row * width] = Random.Range(0, 3);
                    }
                }
                else
                {
                    map[splitPoint + row * width] = Random.Range(0, 3);
                }
            }

            if (!gapDone)
            {
                map[splitPoint + startX * width] = Random.Range(3, 6);
            }
        }

    }

    // Update is called once per frame
    void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Space))
        {
            GenerateMap();
            AssignTileSprites();
        }
	}

    void GenerateTileObjects()
    {
        int offsetX = width / 2;
        int offsetY = height / 2;

        GameObject tiles = new GameObject("_tiles");
        tileObjectList = new GameObject[width * height];

        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                int index = col + row * width;
                tileObjectList[index] = new GameObject();
                tileObjectList[index].transform.parent = tiles.transform;
                SpriteRenderer sr = tileObjectList[index].AddComponent<SpriteRenderer>();
                //sr.sprite = allTiles[map[index]];
                Vector3 loc = new Vector3(col - offsetX, row - offsetY, 0);
                tileObjectList[index].transform.position = loc;
            }
        }
    }
}
