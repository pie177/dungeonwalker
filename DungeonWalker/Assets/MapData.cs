﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapData : MonoBehaviour {

#region "Variables"
    [SerializeField, Tooltip("Width in tiles")]
    int width;

    [SerializeField, Tooltip("Height in tiles")]
    int height;

    [SerializeField, Tooltip("Number of floor tiles")]
    int floorTileNumber;

    [SerializeField]
    Texture2D tileSet;

    [SerializeField]
    int totalTraps = 10;

    [SerializeField]
    float percentWidthMax = 0.9f;
    [SerializeField]
    float percentWidthMin = 0.7f;

    [SerializeField]
    float percentHeightMax = 0.9f;
    [SerializeField]
    float percentHeightMin = 0.7f;

    public Sprite[] allTiles;

    public int[] map;

    GameObject[] tileObjectList;

    int offsetX;
    int offsetY;

    [HideInInspector]
    public int startX;
    [HideInInspector]
    public int startY;

    public Vector3 startPos = Vector3.zero;

#endregion

    // Use this for initialization
    void Start ()
    {
        allTiles = Resources.LoadAll<Sprite>(tileSet.name);

        //GenerateMap();
        GenerateCave();

        GenerateTileObjects();
        AssignTileSprites();

        SetStartPos();
	}

    private void AssignTileSprites()
    {
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                int i = col + row * width;

                tileObjectList[i].GetComponent<SpriteRenderer>().sprite = allTiles[map[i]];
            }
        }
    }

    public int GetWidth()
    {
        return width;
    }

    public int GetHeight()
    {
        return height;
    }

    private void GenerateMap()
    {
        map = new int[width * height];

        #region "FirstPass"
        //first pass
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                map[col + row * width] = Random.Range(0, 3);
            }
        }

        #endregion

        #region "SecondPass"
        //second pass
        int roomWidth = Mathf.FloorToInt(width * Random.Range(percentWidthMin, percentWidthMax));
        int roomHeight = Mathf.FloorToInt(width * Random.Range(percentHeightMin, percentHeightMax));

        roomWidth = Mathf.Clamp(roomWidth, 1, width - 1);
        roomHeight = Mathf.Clamp(roomHeight, 1, height - 1);

        int startX = (width - roomWidth) / 2;
        int startY = (height - roomHeight) / 2;

        for (int row = startY; row < roomHeight; row++)
        {
            for (int col = startX; col < roomWidth; col++)
            {
                map[col + row * width] = Random.Range(3, 6);
            }
        }
        #endregion

        #region "ThirdPass"
        //third pass
        bool left = false;
        if(Random.value > 0.5f)
        {
            left = true;
        }

        if(left)
        {
            bool gapDone = false;
            int splitPoint = roomHeight / 2 + startY;
            for(int col = startX; col < (startX + roomWidth); col++)
            {
                if(!gapDone)
                {
                    if(Random.value < .15)
                    {
                        gapDone = true;
                    }
                    else
                    {
                        map[col + splitPoint * width] = Random.Range(0, 3);
                    }
                }
                else
                {
                    map[col + splitPoint * width] = Random.Range(0, 3);
                }
            }

            if(!gapDone)
            {
                map[startX + splitPoint * width] = Random.Range(3, 6);
            }
        }

        else
        {
            bool gapDone = false;
            int splitPoint = roomWidth / 2 + startX;
            for (int row = startY; row < (startY + roomHeight); row++)
            {
                if (!gapDone)
                {
                    if (Random.value < .15)
                    {
                        gapDone = true;
                    }
                    else
                    {
                        map[splitPoint + row * width] = Random.Range(0, 3);
                    }
                }
                else
                {
                    map[splitPoint + row * width] = Random.Range(0, 3);
                }
            }

            if (!gapDone)
            {
                map[splitPoint + startY * width] = Random.Range(3, 6);
            }
        }
        #endregion

        #region "FourthPass"

        //place traps
        int trapsPlaced = 0;
        int rounds = 0;

        while(trapsPlaced < totalTraps && rounds < 10000)
        {
            rounds++;

            int index = Random.Range(0, map.Length);

            if(CheckIfFloor(index))
            {
                map[index] = 8;
                trapsPlaced++;
            }
        }

        #endregion

        #region "Fifth Pass"
        
        //adding start and end points
        for(int i = 0; i < map.Length; i++)
        {
            if(CheckIfFloor(i) || CheckIfTrap(i))
            {
                i = i + 1 + width;
                map[i] = 6;
                break;
            }
        }

        //int tileNumber = startX + roomWidth - 2 + ((startY + roomHeight - 2) * width);
        //map[tileNumber] = 7;

        for (int i = map.Length - 1; i > 0; i--)
        {
            if (CheckIfFloor(i) || CheckIfTrap(i))
            {
                i = i - 1 - width;
                map[i] = 7;
                break;
            }
        }

        #endregion

    }

    private void GenerateCave()
    {
        #region First Pass

        map = new int[width * height];

        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                map[col + row * width] = Random.Range(0, 3);
            }
        }
        #endregion

        #region Second Pass
        do
        {
            startX = Random.Range(1, width);
            startY = Random.Range(1, height);
        } while (CheckIfOutsideWall(startX, startY));

        map[startX + startY * width] = 6;

        #endregion

        #region Third Pass

        int counter = 0;
        int unBreaker = 0;
        print(unBreaker);

        int xPos = startX;
        int yPos = startY;

        while (counter < floorTileNumber && unBreaker < 100000)
        {
            int xMove = 0;
            int yMove = 0;

            int dice = Random.Range(0, 4);

            if (dice == 0)
            {
                xMove = 1;
                int tempX = xPos + xMove;
                if(!CheckIfOutsideWall(tempX, yPos))
                {
                    xPos = tempX;

                    if (CheckIfWall(tempX, yPos))
                    {
                        map[tempX + yPos * width] = 3;
                        counter++;
                    }
                }
            }
            else if(dice == 1)
            {
                yMove = 1;
                int tempY = yPos + yMove;
                if (!CheckIfOutsideWall(xPos, tempY))
                {
                    yPos = tempY;

                    if (CheckIfWall(xPos, tempY))
                    {
                        map[xPos + tempY * width] = 3;
                        counter++;
                    }
                }
            }
            else if(dice == 2)
            {
                xMove = -1;
                int tempX = xPos + xMove;
                if (!CheckIfOutsideWall(tempX, yPos))
                {
                    xPos = tempX;

                    if (CheckIfWall(tempX, yPos))
                    {
                        map[tempX + yPos * width] = 3;
                        counter++;
                    }
                }
            }
            else if(dice == 3)
            {
                yMove = -1;
                int tempY = yPos + yMove;
                if (!CheckIfOutsideWall(xPos, tempY))
                {
                    yPos = tempY;

                    if (CheckIfWall(xPos, tempY))
                    {
                        map[xPos + tempY * width] = 3;
                        counter++;
                    }
                }
            }
            
            //if(!CheckIfOutsideWall(xPos + xMove, yPos + yMove) && map[(yPos + yMove) + (xPos + xMove) * width] != 6)
            //{
            //    xPos += xMove;
            //    yPos += yMove;

            //    if(CheckIfWall(xPos, yPos))
            //    {
            //        map[yPos + xPos * width] = Random.Range(3, 6);
            //        counter++;
            //    }
            //}

            unBreaker++;
        }
        print(counter);

        print(unBreaker);

        #endregion

        #region Fourth Pass
        //place traps
        int trapsPlaced = 0;
        int rounds = 0;

        while (trapsPlaced < totalTraps && rounds < 10000)
        {
            rounds++;

            int index = Random.Range(0, map.Length);

            if (CheckIfFloor(index))
            {
                map[index] = 8;
                trapsPlaced++;
            }
        }
        #endregion

        #region Fifth Pass

        int endX = 0;
        int endY = 0;

        do
        {
            endX = Random.Range(1, width);
            endY = Random.Range(1, height);
        } while (!CheckIfFloor(endX, endY));

        map[endX + endY * width] = 7;

        #endregion
    }

    public int GetRandomFloorTile()
    {
        int index = -1;
        int rounds = 0;

        while(rounds < 10000)
        {
            rounds += 1;

            index = Random.Range(0, map.Length);

            if (CheckIfFloor(index))
            {
                break;
            }
        }

        return index;
        
    }

    public void GetRandomFloorTile(out int x, out int y)
    {
        int index = -1;
        int rounds = 0;

        while (rounds < 10000)
        {
            rounds += 1;

            index = Random.Range(0, map.Length);

            if (CheckIfFloor(index))
            {
                break;
            }
        }

        x = index % width;
        y = index / width;

    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GenerateCave();
            AssignTileSprites();
        }
    }

    void GenerateTileObjects()
    {
        //offsetX = width / 2;
        //offsetY = -height / 2;

        offsetX = 0;
        offsetY = 0;

        GameObject tiles = new GameObject("_tiles");
        tileObjectList = new GameObject[width * height];

        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                int index = col + row * width;
                tileObjectList[index] = new GameObject();
                tileObjectList[index].transform.parent = tiles.transform;
                SpriteRenderer sr = tileObjectList[index].AddComponent<SpriteRenderer>();
                //sr.sprite = allTiles[map[index]];
                Vector3 loc = new Vector3(col - offsetX, -row - offsetY, 0);
                tileObjectList[index].transform.position = loc;
            }
        }
    }

    public bool CheckIfFloor(int index)
    {
        bool result = false;

        if(index < 0 || index >= map.Length)
        {
            result = false;
        }

        else if(map[index] == 3 || map[index] == 4 || map[index] == 5)
        {
            result = true;
        }

        return result;
    }

    public bool CheckIfFloor(int x, int y)
    {
        return CheckIfFloor(x + y * width);
    }

    public bool CheckIfTrap(int index)
    {
        bool result = false;

        if (index < 0 || index >= map.Length)
        {
            result = false;
        }

        else if (map[index] == 8)
        {
            result = true;
        }

        return result;
    }

    public bool CheckIfTrap(int x, int y)
    {
        return CheckIfTrap(x + y * width);
    }

    public bool CheckIfWall(int index)
    {
        bool result = false;

        if (index < 0 || index >= map.Length)
        {
            result = false;
        }

        else if (map[index] == 0 || map[index] == 1 || map[index] == 2)
        {
            result = true;
        }

        return result;
    }

    public bool CheckIfWall(int x, int y)
    {
        return CheckIfWall(x + y * width);
    }

    public bool CheckIfTarget(int index)
    {
        bool result = false;

        if(index < 0 || index >= map.Length)
        {
            result = false;
        }
        else if(map[index] == 7)
        {
            result = true;
        }

        return result;
    }

    public bool CheckIfTarget(int x, int y)
    {
        return CheckIfTarget(x + y * width);
    }

    public bool CheckIfOutsideWall(int x, int y)
    {
        bool result = false;

        if(x == 0 || x == width -1 || y == 0 || y == height - 1)
        {
            result = true;
        }

        return result;
    }

    public void SetStartPos()
    {
        for (int i = 0; i < map.Length; i++)
        {
            if (CheckIfFloor(i) || CheckIfTrap(i))
            {
                i = i + 1 + width;
                startPos = tileObjectList[i].transform.position;
                break;
            }
        }
    }
}
