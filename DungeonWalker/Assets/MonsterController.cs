﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterController : MonoBehaviour {

    public int x, y;

    PlayerControl player;
    MapData dungeonMap;
    GameLogic logic;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        dungeonMap = GameObject.Find("MapData").GetComponent<MapData>();
        logic = GameObject.Find("GameController").GetComponent<GameLogic>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetLocation(int pX, int pY)
    {
        x = pX;
        y = pY;
        transform.position = new Vector3(x, -y, 0);
    }

    public void MonsterTurn()
    {
        int tX = 0;
        int tY = 0;
        int tValue = int.MaxValue;

        int cX = 0;
        int cY = 0;
        int index = 0;

        //up
        cX = x;
        cY = y - 1;
        index = cX + cY * dungeonMap.GetWidth();
        if(player.dMap[index] < tValue)
        {
            tValue = player.dMap[index];
            tX = cX;
            tY = cY;
        }

        //right
        cX = x + 1;
        cY = y;
        index = cX + cY * dungeonMap.GetWidth();
        if (player.dMap[index] < tValue)
        {
            tValue = player.dMap[index];
            tX = cX;
            tY = cY;
        }

        //down
        cX = x;
        cY = y + 1;
        index = cX + cY * dungeonMap.GetWidth();
        if (player.dMap[index] < tValue)
        {
            tValue = player.dMap[index];
            tX = cX;
            tY = cY;
        }

        //left
        cX = x - 1;
        cY = y;
        index = cX + cY * dungeonMap.GetWidth();
        if (player.dMap[index] < tValue)
        {
            tValue = player.dMap[index];
            tX = cX;
            tY = cY;
        }

        if(tX != 0 && tY != 0)
        {
            SetLocation(tX, tY);
            if(tValue == 0)
            {
                if(Random.Range(1, 100) <= 50)
                {
                    player.ChangeHealth(-1);
                }
                logic.DestroyMonster(this);
            }
        }
    }

    public bool AtLocation(int tX, int tY)
    {
        if(tX == x && tY == y)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
