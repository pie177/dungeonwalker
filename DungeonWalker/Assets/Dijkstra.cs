﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dijkstra {

    static int width;
    static int height;

	public static int[] GenerateMap(int pWidth, int pHeight, int goalX, int goalY, MapData mapData, int[] oldMap = null)
    {
        width = pWidth;
        height = pHeight;

        int[] map;

        if(oldMap == null)
        {
            map = new int[width * height];
        }
        else
        {
            map = oldMap;
        }

        for(int i = 0; i < map.Length; i++)
        {
            map[i] = int.MaxValue;
        }

        int goalIndex = goalX + goalY * width;
        map[goalIndex] = 0;

        Queue<int> openNodes = new Queue<int>();
        openNodes.Enqueue(goalIndex);

        while(openNodes.Count > 0)
        {
            int currentIndex = openNodes.Dequeue();

            int curX = currentIndex % width;
            int curY = currentIndex / width;

            int tX, tY;

            //checking node
            //up
            tX = curX;
            tY = curY - 1;
            CheckNode(map, openNodes, currentIndex, tX, tY, mapData);

            //down
            tX = curX;
            tY = curY + 1;
            CheckNode(map, openNodes, currentIndex, tX, tY, mapData);

            //right
            tX = curX + 1;
            tY = curY;
            CheckNode(map, openNodes, currentIndex, tX, tY, mapData);


            //left
            tX = curX - 1;
            tY = curY;
            CheckNode(map, openNodes, currentIndex, tX, tY, mapData);
        }

        return map;
    }

    private static int CheckNode(int[] map, Queue<int> openNodes, int currentIndex, int tX, int tY, MapData mapData)
    {
        int targetIndex = -1;

        if (IsValidIndex(tX, tY))
        {
            if (mapData.CheckIfFloor(tX, tY))
            {
                targetIndex = tX + tY * width;
                if (map[targetIndex] > (map[currentIndex] + 2))
                {
                    map[targetIndex] = map[currentIndex] + 1;
                    openNodes.Enqueue(targetIndex);
                }
            }
        }

        return targetIndex;
    }

    static bool IsValidIndex(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }
}
