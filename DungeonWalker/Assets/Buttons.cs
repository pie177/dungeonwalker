﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void _Start()
    {
        SceneManager.LoadScene(1);
    }

    public void _Quit()
    {
        Application.Quit();
    }

    public void _Continue()
    {
        SceneManager.LoadScene(0);
    }
}
