﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour {

    [SerializeField]
    int monsterCount = 1;

    [SerializeField]
    GameObject monsterTemplate;

    public List<MonsterController> monsters;

    PlayerControl player;
    MapData map;

    bool isPlayerTurn = true;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
        map = GameObject.Find("MapData").GetComponent<MapData>();

        monsters = new List<MonsterController>();

        for(int i = 0; i < monsterCount; i++)
        {
            MonsterController temp = GameObject.Instantiate(monsterTemplate).GetComponent<MonsterController>();
            int x, y;
            map.GetRandomFloorTile(out x, out y);
            temp.SetLocation(x, y);
            monsters.Add(temp);
        }
	}

    public MonsterController HasMonster(int x, int y)
    {
        MonsterController result = null;

        for(int i = 0; i < monsters.Count; i++)
        {
            if(monsters[i].AtLocation(x, y))
            {
                result = monsters[i];
                break;
            }
        }

        return result;
    }

    public void DestroyMonster(MonsterController target)
    {
        monsters.Remove(target);
        Destroy(target.gameObject);
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(isPlayerTurn == true)
        {
            if(player.PlayerTurn())
            {
                player.moved = !player.moved;
                isPlayerTurn = player.moved;
            }            
        }
        else
        {
            MonsterTurn();
            isPlayerTurn = !isPlayerTurn;
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            //MonsterTurn();
        }
	}


    public void MonsterTurn()
    {
        for(int i = 0; i < monsters.Count; i++)
        {
            monsters[i].MonsterTurn();
        }
    }
}
