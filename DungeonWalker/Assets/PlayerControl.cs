﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControl : MonoBehaviour {

    public int x = 0;
    public int y = 0;

    MapData mapData;

    public int maxHealth = 5;
    int currentHealth;

    public int[] dMap;

    UnityEngine.UI.Text hpText;

    GameLogic logic;

    public bool moved;

    // Use this for initialization
    void Start ()
    {
        mapData = GameObject.Find("MapData").GetComponent<MapData>();
        logic = GameObject.Find("GameController").GetComponent<GameLogic>();
        
        x = mapData.startX;
        y = mapData.startY;

        transform.position = new Vector3(x, -y, 0);

        currentHealth = maxHealth;

        hpText = GameObject.Find("HealthText").GetComponent<UnityEngine.UI.Text>();

        dMap = Dijkstra.GenerateMap(mapData.GetWidth(), mapData.GetHeight(), x, y, mapData);
	}
	
	// Update is called once per frame
	public bool PlayerTurn ()
    {
        bool result = moved;

        if(result)
        {
            CheckMonster();
        }

        UpdateHP();

        dMap = Dijkstra.GenerateMap(mapData.GetWidth(), mapData.GetHeight(), x, y, mapData);

        return result;
    }

    private void CheckMonster()
    {
        MonsterController target = logic.HasMonster(x, y);

        if(target != null)
        {
            if(Random.Range(1, 100) <= 15)
            {
                currentHealth--;
            }

            logic.DestroyMonster(target);
            print("Player Attacked Monster");
        }
    }

    public void UpdateHP()
    {
        hpText.text = string.Format("HP: {1}/{0}", maxHealth, currentHealth);
    }

    /*public bool Move()
    {
        int targetX = x;
        int targetY = y;

        bool moved = false;

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            targetY -= 1;

            moved = true;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            targetY += 1;

            moved = true;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            targetX -= 1;

            moved = true;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            targetX += 1;

            moved = true;
        }

        if (!mapData.CheckIfWall(targetX, targetY) && moved)
        {
            x = targetX;
            y = targetY;

            moved = true;
        }

        if (mapData.CheckIfTrap(x, y) && moved)
        {
            currentHealth--;

            if (currentHealth <= 0)
            {
                SceneManager.LoadScene(2);
            }
        }

        if (mapData.CheckIfTarget(x, y) && moved)
        {
            SceneManager.LoadScene(3);
        }

        transform.position = new Vector3(x, -y, 0);

        return moved;
    }*/

    public void ChangeHealth(int amount)
    {
        currentHealth += amount;
    }

    public void _MoveUp()
    {
        int targetX = x;
        int targetY = y;

        targetY -= 1;

        moved = true;

        if (!mapData.CheckIfWall(targetX, targetY) && moved)
        {
            x = targetX;
            y = targetY;

            moved = true;
        }

        if (mapData.CheckIfTrap(x, y) && moved)
        {
            currentHealth--;

            if (currentHealth <= 0)
            {
                SceneManager.LoadScene(2);
            }
        }

        if (mapData.CheckIfTarget(x, y) && moved)
        {
            SceneManager.LoadScene(3);
        }

        transform.position = new Vector3(x, -y, 0);
    }
    public void _MoveDown()
    {
        int targetX = x;
        int targetY = y;

        targetY += 1;

        moved = true;

        if (!mapData.CheckIfWall(targetX, targetY) && moved)
        {
            x = targetX;
            y = targetY;

            moved = true;
        }

        if (mapData.CheckIfTrap(x, y) && moved)
        {
            currentHealth--;

            if (currentHealth <= 0)
            {
                SceneManager.LoadScene(2);
            }
        }

        if (mapData.CheckIfTarget(x, y) && moved)
        {
            SceneManager.LoadScene(3);
        }

        transform.position = new Vector3(x, -y, 0);
    }

    public void _MoveLeft()
    {
        int targetX = x;
        int targetY = y;

        targetX -= 1;

        moved = true;

        if (!mapData.CheckIfWall(targetX, targetY) && moved)
        {
            x = targetX;
            y = targetY;

            moved = true;
        }

        if (mapData.CheckIfTrap(x, y) && moved)
        {
            currentHealth--;

            if (currentHealth <= 0)
            {
                SceneManager.LoadScene(2);
            }
        }

        if (mapData.CheckIfTarget(x, y) && moved)
        {
            SceneManager.LoadScene(3);
        }

        transform.position = new Vector3(x, -y, 0);
    }

    public void _MoveRight()
    {
        int targetX = x;
        int targetY = y;

        targetX += 1;

        moved = true;

        if (!mapData.CheckIfWall(targetX, targetY) && moved)
        {
            x = targetX;
            y = targetY;

            moved = true;
        }

        if (mapData.CheckIfTrap(x, y) && moved)
        {
            currentHealth--;

            if (currentHealth <= 0)
            {
                SceneManager.LoadScene(2);
            }
        }

        if (mapData.CheckIfTarget(x, y) && moved)
        {
            SceneManager.LoadScene(3);
        }

        transform.position = new Vector3(x, -y, 0);
    }
}
